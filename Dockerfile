FROM alpine:3.6
MAINTAINER ashareye <ashareye@aliyun.com> 

COPY esp-open-sdk.zip /tmp/esp-open-sdk.zip
COPY tarballs.zip /tmp/tarballs.zip
COPY gnu-patch /bin/patch

ENV PACKAGES="build-base unrar autoconf automake libtool gcc g++ gperf flex bison texinfo gawk ncurses-dev expat-dev \
python2-dev python2 py2-pip sed git unzip bash help2man wget bzip2 shadow libc-dev"

RUN echo "https://mirror.tuna.tsinghua.edu.cn/alpine/v3.6/main" > /etc/apk/repositories && \
    echo "https://mirror.tuna.tsinghua.edu.cn/alpine/v3.6/community" >> /etc/apk/repositories && \

    apk update --no-cache && \
    apk --update --no-cache add make && \
    apk --update --no-cache add --virtual build-temp $PACKAGES && \
    rm -rf /var/lib/apt/lists/* && \
    pip install pyserial && \

    unzip /tmp/esp-open-sdk.zip -d / && \
    rm -rf /tmp/esp-open-sdk.zip && \
    mkdir /esp-open-sdk/crosstool-NG/.build && \
    unzip /tmp/tarballs.zip -d /esp-open-sdk/crosstool-NG/.build && \
    rm -rf /tmp/tarballs.zip && \
    unlink /usr/bin/patch && \
    ln -s /bin/patch /usr/bin/patch && \

    adduser -D espuser && \
    chown -R espuser.espuser /esp-open-sdk

USER espuser

RUN cd /esp-open-sdk && make && \
    apk del build-temp

ENV PATH /esp-open-sdk/xtensa-lx106-elf/bin/:$PATH


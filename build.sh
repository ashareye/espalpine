echo "https://mirror.tuna.tsinghua.edu.cn/alpine/v3.6/main" > /etc/apk/repositories && \
echo "https://mirror.tuna.tsinghua.edu.cn/alpine/v3.6/community" >> /etc/apk/repositories && \
apk update --no-cache && \
apk --update --no-cache add git autoconf automake build-base gperf bison flex texinfo libtool \
ncurses-dev wget gawk libc-dev help2man sed unzip python2-dev python2 py2-pip && \
pip install pyserial && \
cp /working/gnu-patch /bin/patch && \
unlink /usr/bin/patch && \
ln -s /bin/patch /usr/bin/patch && \
unzip /working/esp-open-sdk.zip -d / && \
mkdir /esp-open-sdk/crosstool-NG/.build && \
unzip /working/tarballs.zip -d /esp-open-sdk/crosstool-NG/.build && \
adduser -D espuser && \
chown -R espuser.espuser /esp-open-sdk
su espuser